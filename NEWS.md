NEWS
====
Here you will find a high level list of new features and bugfixes for each releases. 

switcher-jack 0.0.4 (2023-05-09)
---------------------------

* add release command in README
* allow index 0 for jacksrc

switcher-jack 0.0.2 (2023-04-18)
---------------------------

* Initial works-on-my-computer version

