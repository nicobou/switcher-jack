# Release

Make a new release of this repository:
```
./switcher-plugin-builder/release_switcher_plugin -l switcher-jack -r git@gitlab.com:nicobou
```

